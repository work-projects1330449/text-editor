package ru.luminitetime.texteditorfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;

public class TextEditorApp extends Application {
    public static Stage STAGE;

    @Override
    public void start(Stage stage) throws IOException {
        STAGE = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(TextEditorApp.class.getResource("text-editor-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1250, 800);
        stage.setTitle("Text Editor");
        stage.setScene(scene);
        stage.show();

        scene.setOnKeyPressed(e -> {
            if (e.isControlDown()) {
                if (e.getCode() == KeyCode.S) {
                    // TODO: Save file
                    System.out.println(12334454);
                }
            }
        });
    }

    public static void main(String[] args) {
        launch();
    }
}