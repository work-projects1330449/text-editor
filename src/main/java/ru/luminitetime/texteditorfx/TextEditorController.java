package ru.luminitetime.texteditorfx;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.util.Duration;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

import static ru.luminitetime.texteditorfx.TextEditorApp.STAGE;

public class TextEditorController implements Initializable {
    @FXML
    private TextArea textArea;
    @FXML
    private Label savedLabel;
    @FXML
    private ToggleButton italicButton;
    @FXML
    private ToggleButton boldButton;

    private static String fontFamilyNow;
    private static FontPosture fontPostureNow;
    private static FontWeight fontWeightNow;
    private static double fontSizeNow;

    private static BufferedReader fileReader;
    private static File nowFile = null;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fontFamilyNow = textArea.getFont().getFamily();
        fontPostureNow = FontPosture.REGULAR;
        fontWeightNow = FontWeight.NORMAL;
        fontSizeNow = textArea.getFont().getSize();
    }

    @FXML
    public void saveFileAs() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save this file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setInitialFileName("untitled.txt");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt")
        );
        File file = fileChooser.showSaveDialog(STAGE);
        if (file == null) {
            return;
        }

        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
        fileWriter.write(textArea.getText());
        fileWriter.close();

        nowFile = file;

        showSavedWindow();
    }

    @FXML
    public void saveFile() throws IOException {
        if (nowFile == null) {
            saveFileAs();
            return;
        }
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(nowFile));
        fileWriter.write(textArea.getText());
        fileWriter.close();

        showSavedWindow();
    }

    @FXML
    public void openFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose text file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt")
        );
        File file = fileChooser.showOpenDialog(STAGE);
        if (file == null) {
            return ;
        }

        fileReader = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder textFromOpenedFile = new StringBuilder();
        while ((line = fileReader.readLine()) != null) {
            textFromOpenedFile.append(line).append("\n");
        }
        textArea.setText(textFromOpenedFile.toString());

        nowFile = file;
    }

    @FXML
    public void setItalicFont() {
        if (italicButton.isSelected()) {
            fontPostureNow = FontPosture.ITALIC;
        } else {
            fontPostureNow = FontPosture.REGULAR;
        }
        Font newFont = Font.font(fontFamilyNow, fontWeightNow, fontPostureNow, fontSizeNow);
        textArea.setFont(newFont);
    }

    @FXML
    public void setBoldFont() {
        if (boldButton.isSelected()) {
            fontWeightNow = FontWeight.BOLD;
        } else {
            fontWeightNow = FontWeight.NORMAL;
        }
        Font newFont = Font.font(fontFamilyNow, fontWeightNow, fontPostureNow, fontSizeNow);
        textArea.setFont(newFont);
    }

    @FXML
    public void quitApp() {
        Platform.exit();
    }

    public void showSavedWindow() {
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1), savedLabel);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);
        fadeTransition.setCycleCount(2);
        fadeTransition.setAutoReverse(true);
        fadeTransition.play();
    }

    @FXML
    public void createNewFile() {
        textArea.setText("");
    }
}