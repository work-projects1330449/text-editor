module ru.luminitetime.texteditorfx {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;

    opens ru.luminitetime.texteditorfx to javafx.fxml;
    exports ru.luminitetime.texteditorfx;
}